import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "cases",
    component: () => import("../pages/cases/MainPage.vue")
  },
  {
    path: "/case-details",
    name: "case-details",
    component: () => import("../pages/cases/CaseDetailsPage.vue")
  },
  {
    path: "/people",
    name: "people",
    component: () => import("../pages/cases/PeoplePage.vue")
  },
  {
    path: "/files",
    name: "files",
    component: () => import("../pages/cases/FilesPage.vue")
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../components/auth/Login.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
